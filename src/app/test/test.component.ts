import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Test } from '../shared/test.model';
import { TestService } from '../shared/test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {
  @Input() test!: Test;
  @Output() newTest = new EventEmitter<Test>();

  constructor(public testService: TestService){}

  userInput = '';
  answer = '';
  help = '';

  tries = 0;

  onSubmit(){
    const test = new Test(this.userInput, this.answer, this.help);
    this.newTest.emit(test);
  }

  countTries(){
    if(this.userInput === this.answer){
      this.tries ++
    }
  }
}
