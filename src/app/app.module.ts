import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { TestItemComponent } from './test-item/test-item.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ModalComponent } from './ui/modal/modal.component';
import { TestService } from './shared/test.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    TestItemComponent,
    ToolbarComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [TestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
