import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Test } from '../shared/test.model';
import { TestService } from '../shared/test.service';

@Component({
  selector: 'app-test-item',
  templateUrl: './test-item.component.html',
  styleUrls: ['./test-item.component.css']
})
export class TestItemComponent {
  @Input() test!: Test;
  @Output() newTestItem = new EventEmitter<Test>();
  @ViewChild('userInput') userInput!: ElementRef;
  modalOpen = false;
  tries = 0;

constructor(public testService: TestService){}


openHelpModal(){
    this.modalOpen = true;
  }

  closeHelpModal(){
    this.modalOpen = false;
  }

  createAnswer(){
    const userAnswer: string = this.userInput.nativeElement.value;
    const question: string = this.userInput.nativeElement.value;
    const help: string = this.userInput.nativeElement.value;
    const test = new Test(question, userAnswer, help);

    if(userAnswer === test.answer){
      return 'right'
    } else{
      return 'false'
    }

    if(userAnswer !== test.answer){
      return this.tries ++;
    }

  }

}
